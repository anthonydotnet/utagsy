﻿namespace uTagsy.Web.Controllers.WebApi
{
    using System;

    using Umbraco.Core.Models;

    using System.Collections.Generic;
    using System.Linq;

    using Helpers;

    using uHelpsy.Comparers;
    using uHelpsy.Helpers;

    public class uTagsyApiController : Umbraco.Web.WebApi.UmbracoAuthorizedApiController
    {
        /// <summary>
        /// Gets all tags under tag container
        /// </summary>
        /// <param name="currentNodeId"></param>
        /// <returns></returns>
        public IEnumerable<string> GetAllTags(int currentNodeId)
        {
            var doc = this.Services.ContentService.GetById(currentNodeId);

            IContent container = IContentHelper.GetIContentByAlias(doc, ConfigReader.Instance.GetRootNodeTypeAlias(), ConfigReader.Instance.GetTagContainerNodeTypeAlias());

            var propertyAlias = ConfigReader.Instance.GetTagNamePropertyAlias();

            var tags = container.Children().Where(x => x.HasProperty(propertyAlias)).Select(x => x.GetValue<string>(ConfigReader.Instance.GetTagNamePropertyAlias()));

            return tags;
        }




        /// <summary>
        /// Gets tag names for given node ids.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<string> GetTagNames(string nodeIds)
        {
            if (string.IsNullOrWhiteSpace(nodeIds))
            {
                return new List<string>();
            }
            var ids = nodeIds.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries).Select(int.Parse);
            var nodes = this.Services.ContentService.GetByIds(ids);

            var propertyAlias = ConfigReader.Instance.GetTagNamePropertyAlias();

            var values = nodes.Where(x => x.HasProperty(propertyAlias)).Select(x => x.GetValue<string>(ConfigReader.Instance.GetTagNamePropertyAlias()));

            return values;
        }



        /// <summary>
        /// Creates nodes for tags
        /// </summary>
        /// <returns></returns>
        public IEnumerable<int> GetAndEnsureNodeIdsForTags(string currentNodeId, string tags)
        {
            if (string.IsNullOrWhiteSpace(tags))
            {
                return new List<int>();
            }

            // store alias so we don't have to use this verbose thing
            var tagPropAlias = ConfigReader.Instance.GetTagNamePropertyAlias();

            // put posted tags in an array
            var postedTags = tags.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

            // get the current node
            var node = this.Services.ContentService.GetById(int.Parse(currentNodeId));

            // get all existing tag nodes in container
            var tagContainer = this.GetTagContainer(node);
            var allTagNodes = tagContainer.Children();

            var hasNewTags = false;
            foreach (var postedTag in postedTags)
            {
                // get tag names which do not already exist in the tag container
                var found = allTagNodes.Any(x => x.GetValue<string>(tagPropAlias) == postedTag);
                if (!found)
                {
                    // tag node doesnt exist so create new node
                    var dic = new Dictionary<string, object>() { { tagPropAlias, postedTag } };
                    IContentHelper.CreateContentNode(postedTag, ConfigReader.Instance.GetTagNodeTypeAlias(), dic, tagContainer.Id, true);
                    hasNewTags = true;
                }
            }

            // re-get container because new nodes might have been added.
            tagContainer = this.GetTagContainer(node);
            if (hasNewTags)
            {
                // new tag so sort!
                IContentHelper.SortNodes(tagContainer.Id, this.Services.ContentService.GetChildren(tagContainer.Id), new IContentComparer());
            }

            // get all tag ids, and return
            var tagIds = tagContainer
                                .Children()
                                .Where(x => postedTags.Contains(x.GetValue<string>(tagPropAlias)))
                                .Select(x => x.Id);

            return tagIds;
        }



        /// <summary>
        /// Traverses up the tree, then down again to get the tag container.
        /// </summary>
        /// <returns></returns>
        protected IContent GetTagContainer(IContent content)
        {
            return IContentHelper.GetIContentByAlias(content, ConfigReader.Instance.GetRootNodeTypeAlias(), ConfigReader.Instance.GetTagContainerNodeTypeAlias());
        }

    }
}